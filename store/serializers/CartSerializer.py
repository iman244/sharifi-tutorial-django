from rest_framework import serializers
from store.models.Cart import Cart
from users.serializers import CustomUserSerializer
from .ProductSerializer import ProductSerializer


class CartSerializer(serializers.ModelSerializer):
    user = CustomUserSerializer(read_only=True)
    products = ProductSerializer(read_only=True, many=True)

    class Meta:
        model = Cart
        fields = '__all__'
