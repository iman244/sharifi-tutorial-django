from rest_framework import serializers
from store.models.Payment import Payment
from .ProductSerializer import ProductSerializer
from users.serializers import CustomUserSerializer


class PaymentSerializer(serializers.ModelSerializer):
    products = ProductSerializer(read_only=True, many=True)
    user = CustomUserSerializer(read_only=True)

    class Meta:
        model = Payment
        fields = '__all__'
