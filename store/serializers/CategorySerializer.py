from rest_framework import serializers
from store.models.Category import Category
from .ImageSerializer import ImageSerializer


class CategorySerializer(serializers.ModelSerializer):
    image = ImageSerializer(read_only=True)

    class Meta:
        model = Category
        fields = '__all__'
