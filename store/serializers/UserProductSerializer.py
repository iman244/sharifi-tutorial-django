from rest_framework import serializers
from store.models.UserProduct import UserProduct
from .PaymentSerializer import PaymentSerializer
from .ProductSerializer import ProductSerializer_includeFile


class UserProductSerializer(serializers.ModelSerializer):
    product = ProductSerializer_includeFile(read_only=True)

    class Meta:
        model = UserProduct
        fields = ('product',)
