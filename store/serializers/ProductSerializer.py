from rest_framework import serializers
from store.models.Product import Product
from .FileSerializer import FileSerializer
from .ImageSerializer import ImageSerializer


class ProductSerializer(serializers.ModelSerializer):
    image = ImageSerializer(read_only=True)

    class Meta:
        model = Product
        exclude = ('file',)


class ProductSerializer_includeFile(serializers.ModelSerializer):
    image = ImageSerializer(read_only=True)
    file = FileSerializer(read_only=True)

    class Meta:
        model = Product
        fields = '__all__'
