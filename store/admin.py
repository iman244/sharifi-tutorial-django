from django.contrib import admin
from store.models.Category import Category
from store.models.Product import Product
from store.models.Image import Image
from store.models.File import File
from store.models.Payment import Payment
from store.models.UserProduct import UserProduct


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    # readonly_fields = ['users']
    list_display = ('name', 'id', 'price', 'discount')


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    readonly_fields = ['authority', 'amount',
                       'status', 'paymentStartTime']
    list_display = ('user', 'authority', 'amount',  'status')


@admin.register(UserProduct)
class UserProductAdmin(admin.ModelAdmin):
    readonly_fields = ['user',  'payment']
    list_display = ('user', 'payment')


# Register your models here.
admin.site.register(File)
admin.site.register(Image)
admin.site.register(Category)
