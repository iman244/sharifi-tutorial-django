from django.urls import path
from store.views import FreeProductView, ProductList, ProductDetail, CategoryList, PurchaseProduct, VerifyPurchase, VerifyPayment, CartView
from rest_framework.routers import DefaultRouter

app_name = 'store'

urlpatterns = [
    path('category', CategoryList.as_view(), name='category_list'),
    path('purchase', PurchaseProduct.as_view(), name='purchase'),
    # this url use after payment gate and verify payment and redirect user to front
    path('verify-purchase', VerifyPurchase.as_view(), name='purchase'),
    # this url use from front to verify a payment and display information of the user payment
    path('verify-payment', VerifyPayment.as_view(), name='purchase'),
    path('products/<int:pk>', ProductDetail.as_view(), name='product_detail'),
    path('products/', ProductList.as_view(), name='product_list'),
    path('cart/', CartView.as_view(), name='cart'),
    path('free/', FreeProductView.as_view(), name='free'),
]


# urlpatterns += router.urls
