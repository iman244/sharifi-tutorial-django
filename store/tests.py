from django.test import TestCase
from . import models
from django.contrib.auth.models import User


class ProductTests(TestCase):

    def test_str_product_and_category(self):
        test_user1 = User.objects.create(
            username='testuser1', password='123456789')
        test_category = models.Category.objects.create(name='test category')
        test_product = models.Product.objects.create(
            name='test category', content="test content 1", slug="test-category-1", category=test_category)
        product = models.Product.objects.get(name=test_product.name)
        category = models.Category.objects.get(name=test_category.name)
        self.assertEqual(str(product), test_product.name)
        self.assertEqual(str(category), test_category.name)
