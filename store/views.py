from rest_framework import generics
from .models.Product import Product
from .models.Category import Category
from .models.Payment import Payment
from .models.Cart import Cart
from .models.UserProduct import UserProduct
from .serializers import ProductSerializer, CategorySerializer, PaymentSerializer
from .serializers.CartSerializer import CartSerializer
from rest_framework import permissions, authentication, viewsets
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework.response import Response
from .utils import price
from sharifi_tutorial_backend.settings import ZARINPAL_MERCHANT_ID
from suds.client import Client
from django.http import HttpResponseRedirect
from rest_framework import status


class CategoryList(generics.ListAPIView):
    permission_classes = [permissions.AllowAny]
    authentication_classes = []
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ProductList(generics.ListAPIView):
    permission_classes = [permissions.AllowAny]
    authentication_classes = []
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def get_queryset(self):

        category_name = self.request.query_params.get('category')
        product_name = self.request.query_params.get('name')

        if category_name is None and product_name is None:
            return Product.objects.all()

        if category_name:
            category = Category.objects.get(name=category_name)

            return Product.objects.filter(
                name__contains=product_name, category=category)

        else:
            return Product.objects.filter(
                name__contains=product_name)


class ProductDetail(generics.RetrieveAPIView):
    permission_classes = [permissions.AllowAny]
    authentication_classes = []
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class CartView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk=None):

        # retrive user
        user = request.user

        # retrive user cart in database
        try:
            cart = Cart.objects.get(user=user)

        # if cart for user doesnt existed, so we will create it for him
        except ObjectDoesNotExist:
            cart = Cart.objects.create(user=user)
            cart.save()

        # serialize and return cart
        serializer = CartSerializer(cart)
        return Response(serializer.data)

    def put(self, request, pk=None):
        # retrive user
        user = request.user

        # get order of products
        requested_products = request.data['products']
        intention = request.data['intention']

        # retrive products from order
        try:
            retrived_products = [Product.objects.get(
                name=product_name) for product_name in requested_products]

        # if we don't had such a product raise a error
        except ObjectDoesNotExist:
            return Response({
                "message": f"محصول مورد نظر وجود ندارد"
            })

        # if user previously had product we stop updating cart
        for product in retrived_products:
            owned_products = list(
                UserProduct.objects.filter(user=user).all())
            print("owned_products: ", owned_products)
            if product in owned_products:
                return Response({
                    "message": f"شما محصول {product.name} را قبلاً خریداری کرده‌اید"
                }, status=status.HTTP_409_CONFLICT)

        # retrive user cart from database
        try:
            cart = Cart.objects.get(user=user)

        # if cart for user doesnt existed, so we will create it for him
        except ObjectDoesNotExist:
            cart = Cart.objects.create(user=user)

        # update cart
        for product in retrived_products:
            if intention == 'add':
                cart.products.add(product)
            elif intention == 'remove':
                cart.products.remove(product)
        cart.save()

        # list of products that we added to cart to show in returned message to user
        addedProducts = '، '.join(requested_products)
        if intention == 'add':
            message = f"محصول {addedProducts} به سبد خرید شما اضافه شد"
        elif intention == 'remove':
            message = f"محصول {addedProducts} از سبد خرید شما حذف شد"

        serializer = CartSerializer(cart)
        return Response({
            "message": message,
        })


class PurchaseProduct(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, format=None):

        # retrive user
        user = request.user

        # front showing payment status page to user
        front_callback_url = request.query_params.get('front_callback_url')

        # geting cart of user
        try:
            cart = Cart.objects.get(user=user)

        # if cart for user doesnt existed, so it means user dose not requested form website (because we will build cart from there)
        except ObjectDoesNotExist:
            return Response({
                'message': 'برای خرید از سایت اقدام کنید!'
            })

        requested_products = list(cart.products.all())
        requested_products_names = ', '.join(
            [product.name for product in requested_products])
        prices = [product.price for product in requested_products]
        amount = sum(prices)

        ZARINPAL_WEBSERVICE = 'https://sandbox.zarinpal.com/pg/services/WebGate/wsdl'

        description = requested_products_names
        callback_url = f"http://admin.ieltsonlineshopping.ir/api/store/verify-purchase?front_callback_url={front_callback_url}"

        client = Client(ZARINPAL_WEBSERVICE)
        result = client.service.PaymentRequest(ZARINPAL_MERCHANT_ID,
                                               amount,
                                               description,
                                               '',
                                               '',
                                               callback_url)

        if result.Status == 100:
            payment = Payment.objects.create(
                amount=amount,  authority=result.Authority, user=user)
            for product in requested_products:
                payment.products.add(product)
            payment.save()
            return Response({
                "message": f'در حال انتقال به صفحه پرداخت',
                "payment_gate_url": f'https://sandbox.zarinpal.com/pg/StartPay/{result.Authority}'
            })
        else:
            return Response({
                "message": 'پرداخت با مشکل مواجه شده است، لطفاً مجدداً تلاش کنید',
            })


class VerifyPurchase(APIView):
    permission_classes = [permissions.AllowAny]
    authentication_classes = []
    ZARINPAL_WEBSERVICE = 'https://sandbox.zarinpal.com/pg/services/WebGate/wsdl'

    def get(self, *args, **kwargs):
        Authority = self.request.query_params.get('Authority')
        Status = self.request.query_params.get('Status')
        front_callback_url = self.request.query_params.get(
            'front_callback_url')

        try:
            payment = Payment.objects.get(authority=Authority)

        except ObjectDoesNotExist:
            return f"این درخواست پرداخت ثبت نشده است\nکد {Authority} را به پشتیبانی سایت ارائه دهید و درخواست خود را پیگیری کنید"

        client = Client(self.ZARINPAL_WEBSERVICE)
        result = client.service.PaymentVerification(ZARINPAL_MERCHANT_ID,
                                                    payment.authority,
                                                    payment.amount
                                                    )

        if result.Status == 100 or result.Status == 101:

            # we make payment's status true and start to create user product for each product he had purchased and remove it from his cart
            payment.status = True
            payment.save()

            products = list(payment.products.all())
            cart = Cart.objects.get(user=payment.user)

            for product in products:

                userProduct = UserProduct.objects.create(
                    user=payment.user,  payment=payment, product=product)
                userProduct.save()
                cart.products.remove(product)

            cart.save()

            # at end we redirect him to front page and give front payment id to retrive payment info
            return HttpResponseRedirect(f'{front_callback_url}?paymentId={payment.id}')
        else:
            return HttpResponseRedirect(f'{front_callback_url}?paymentId={payment.id}')


class VerifyPayment(APIView):
    permission_classes = [permissions.IsAuthenticated]
    ZARINPAL_WEBSERVICE = 'https://sandbox.zarinpal.com/pg/services/WebGate/wsdl'

    def get(self, *args, **kwargs):
        # retrive user
        user = self.request.user

        # get paymentId from query paramts
        paymentId = self.request.query_params.get('paymentId')

        # if paymentId is not number for example null because of a error in front then show error
        if not paymentId.isdigit():
            return Response({
                "message": 'درخواست به شکل درستی ارسال نشده است'
            },
                status=status.HTTP_404_NOT_FOUND)

        # get payment from database and if it was not existed or user requested is not user who made payment then show error
        try:
            payment = Payment.objects.get(id=paymentId)
            print("user: ", user)
            print("payment.user: ", payment.user)
            if payment.user != user:
                print('hello, u must not see me')
                return HttpResponseForbidden()

        except ObjectDoesNotExist:
            return Response({
                "text": f"این درخواست پرداخت ثبت نشده است",
            },
                status=status.HTTP_400_BAD_REQUEST)

        client = Client(self.ZARINPAL_WEBSERVICE)
        result = client.service.PaymentVerification(ZARINPAL_MERCHANT_ID,
                                                    payment.authority,
                                                    payment.amount
                                                    )

        if payment.status:
            return Response(PaymentSerializer(payment).data)
        else:
            return Response(PaymentSerializer(payment).data)


class FreeProductView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def put(self, request, pk=None):
        user = request.user

        # get product
        requested_product = request.data['product']

        # retrive products from order
        try:
            retrived_product = Product.objects.get(name=requested_product)

        # if we don't had such a product raise a error
        except ObjectDoesNotExist:
            return Response({
                "message": "محصول مورد نظر وجود ندارد"
            })

        # if product is not free, raise a error
        if retrived_product.price != 0:
            return Response({
                "message": "محصول مورد نظر رایگان نمی‌باشد"
            })

        # if user previously had product we stop updating cart
        owned_products = list(
            UserProduct.objects.filter(user=user).all())
        print("owned_products: ", owned_products)
        if retrived_product in owned_products:
            return Response({
                "message": f"شما محصول {product.name} را قبلاً به حساب کاربری خود اضافه کرده‌اید"
            }, status=status.HTTP_409_CONFLICT)

        # create dummy product
        payment = Payment.objects.create(
            amount=0,  authority="free_product", user=user)
        payment.products.add(retrived_product)
        payment.status = True
        payment.save()

        userProduct = UserProduct.objects.create(
            user=user,  payment=payment, product=retrived_product)
        userProduct.save()

        return Response({
            "message": "محصول مورد نظر به حساب کاربری شما اضافه شد"
        })
