from .models import Product
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response


def price(product_name):

    try:
        product = Product.objects.get(name=product_name)

    except ObjectDoesNotExist:
        return Response({
            'message': f'درخواست خرید محصول {product_name} با خطا مواجه شد'
        })
    return product.price - product.discount
