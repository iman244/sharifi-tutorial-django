from django.db.models.signals import post_delete
from store.models import File, Image
from django.dispatch import receiver


@receiver(post_delete, sender=File)
def delete_from_s3(sender, instance, using, origin, **kwargs):
    instance.file.delete(save=False)


@receiver(post_delete, sender=Image)
def delete_from_s3(sender, instance, using, origin, **kwargs):
    instance.file.delete(save=False)
