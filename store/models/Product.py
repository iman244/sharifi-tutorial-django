from django.db import models
from tinymce import models as tinymce_models
from .File import File
from .Image import Image
from .Category import Category


class Product(models.Model):
    name = models.CharField(max_length=250)
    image = models.ForeignKey(
        Image, on_delete=models.PROTECT)
    file = models.ForeignKey(
        File, on_delete=models.PROTECT)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    content = tinymce_models.HTMLField()
    price = models.PositiveIntegerField()
    discount = models.PositiveIntegerField()

    def __str__(self):
        return self.name
