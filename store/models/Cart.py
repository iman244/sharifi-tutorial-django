from django.db import models
from users.models.CustomUser import CustomUser
from .Product import Product


class Cart(models.Model):
    products = models.ManyToManyField(Product)
    user = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f'{self.user}: {self.id}'
