from django.db import models
from users.models.CustomUser import CustomUser
from .Product import Product
from .Cart import Cart
from .Payment import Payment


class UserProduct(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    payment = models.ForeignKey(Payment, on_delete=models.PROTECT)
