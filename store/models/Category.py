from django.db import models
from .Image import Image


class Category(models.Model):
    name = models.CharField(max_length=100)
    image = models.ForeignKey(Image, on_delete=models.PROTECT)

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.name
