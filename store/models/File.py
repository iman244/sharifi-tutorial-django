from django.db import models
from sharifi_tutorial_backend.storage_backends import PrivateMediaStorage


class File(models.Model):
    name = models.CharField(max_length=100)
    file = models.FileField(
        storage=PrivateMediaStorage())

    def __str__(self):
        return self.name
