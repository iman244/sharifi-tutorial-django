from django.db import models
from sharifi_tutorial_backend.storage_backends import PublicMediaStorage


class Image(models.Model):
    name = models.CharField(max_length=100)
    file = models.FileField(
        storage=PublicMediaStorage())

    def __str__(self):
        return self.name
