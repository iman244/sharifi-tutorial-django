from django.db import models
from users.models.CustomUser import CustomUser
from .Product import Product
from .Cart import Cart
import time


class Payment(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    authority = models.CharField(max_length=250)
    amount = models.PositiveIntegerField()
    status = models.BooleanField(default=False)
    paymentStartTime = models.CharField(max_length=20)
    products = models.ManyToManyField(Product)

    def save(self, *args, **kwargs):
        self.paymentStartTime = time.time()
        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.user}: {self.amount}'
