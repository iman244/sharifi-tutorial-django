from .views import RequestOtpCode, VerifyOtpCode, VerifyUser
from django.urls import path

app_name = 'auth'

urlpatterns = [
    path('request-otp-code/', RequestOtpCode.as_view(), name='request-otp-code'),
    path('verify-otp-code/', VerifyOtpCode.as_view(), name='product_detail'),
    path('verify-user/', VerifyUser.as_view(), name='product_detail'),
]
