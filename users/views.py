from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from .models import CustomUser, Code
import kavenegar
from sharifi_tutorial_backend.settings import KAVENEGAR_API_KEY
import time
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.authtoken.models import Token
from store.models.UserProduct import UserProduct
from store.models.Payment import Payment
from .serializers import CustomUserSerializer
from store.serializers import UserProductSerializer, PaymentSerializer
from rest_framework import status


class RequestOtpCode(APIView):
    permission_classes = [permissions.AllowAny]
    authentication_classes = []

    def post(self, request, format=None):
        phone_number = request.data['phone_number']
        if phone_number[0] == '0':
            phone_number = '+98' + phone_number[1:]

        try:
            user = CustomUser.objects.get(phone_number=phone_number)

        except ObjectDoesNotExist:
            CustomUser.objects.create_user(
                username=phone_number, password='passwordIsOtp', phone_number=phone_number)
            user = CustomUser.objects.get(phone_number=phone_number)

        try:
            code = Code.objects.get(user=user)

            if time.time() - float(code.generatedCodeTime) < 120:
                retryWait = 120 - \
                    int(time.time() - float(code.generatedCodeTime))
                return Response({"message": f'کد برای شما ارسال شده است، {retryWait} ثانیه دیگر می‌توانید مجدداً تلاش کنید'})
            # else:
            #     code.save()
            #     print(code.number)
            #     return Response({
            #         "message": code.number
            #     })

        except ObjectDoesNotExist:
            code = Code.objects.create(user=user)
            # code.save()
            # print(code.number)
            # return Response({
            #     "message": code.number
            # })
        try:
            api = kavenegar.KavenegarAPI(KAVENEGAR_API_KEY)
            code.save()
            print("KavenegarAPI user.phone_number: ", user.phone_number)
            params = {
                'receptor': str(user.phone_number),
                'template': 'ieltsOnlineShopping',
                'token': code.number,
                'type': 'sms',  # sms vs call
            }
            response = api.verify_lookup(params)
            print(response)
            print("code.number: ", code.number)
            return Response({"message": f'کد ورود برای شما پیامک شد'})
        except Exception as e:
            print(type(e))
            print(e.__dir__())
            print(type(e))
            print("kavenegar Exception e: ", e.__str__)
            # .decode(
            #     'unicode-escape').encode('latin1').decode('utf-8')
        # if forgot to add
            return Response({"message": 'لطفاً مجدداً تلاش کنید'}, status=status.HTTP_503_SERVICE_UNAVAILABLE)


class VerifyOtpCode(APIView):
    permission_classes = [permissions.AllowAny]
    authentication_classes = []

    def post(self, request, format=None):
        log = False

        if log:
            print("request.data: ", request.data)
        phone_number = request.data['phone_number']
        claimed_code = request.data['code']

        if phone_number[0] == '0':
            phone_number = '+98' + phone_number[1:]
        if log:
            print('phone_number: ', phone_number)

        try:
            user = CustomUser.objects.get(phone_number=phone_number)
            real_code_object = Code.objects.get(user=user)
            real_code = real_code_object.number
            if log:
                print('user: ', user)
                print('claimed_code: ', claimed_code)
                print('real_code: ', real_code)

            if time.time() - float(real_code_object.generatedCodeTime) > 120:
                return Response({"message": 'کد ورود منقضی شده است'})
            # if int(real_code) == int(claimed_code):
            elif real_code == claimed_code:
                token, created = Token.objects.get_or_create(user=user)

                if log:
                    print('token: ', token)

                return Response({"message": 'شما با موفقیت وارد شدید', 'token': token.key})

        except ObjectDoesNotExist:
            return Response({"message": 'کد ورود  صحیح نیست'})


class VerifyUser(APIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = CustomUserSerializer

    def get(self, request, format=None):
        user = request.user
        products = UserProduct.objects.filter(
            user=user)

        payments = Payment.objects.filter(
            user=user
        )

        return Response({
            "user": {
                "phone_number": request.user.phone_number.__str__(),
                "products": [UserProductSerializer(product).data['product'] for product in list(products.all())],
                'payments': [PaymentSerializer(payment).data for payment in list(payments.all())]
            }
        })
