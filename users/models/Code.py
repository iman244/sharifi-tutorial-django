from django.db import models
from .CustomUser import CustomUser
import random
import time


class Code(models.Model):
    number = models.CharField(max_length=5, blank=True)
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    generatedCodeTime = models.CharField(max_length=20)

    def __str__(self):
        return str(self.number)

    def save(self, *args, **kwargs):
        number_list = [number for number in range(10)]
        pick_list = []
        for i in range(5):
            pick = random.choice(number_list)
            pick_list.append(pick)

        code_string = "".join(str(pick) for pick in pick_list)
        self.number = code_string
        self.generatedCodeTime = time.time()
        super().save(*args, **kwargs)
