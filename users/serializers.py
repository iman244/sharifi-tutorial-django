from rest_framework import serializers
from .models.CustomUser import CustomUser
# from store.serializers import CartSerializer


class CustomUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomUser
        fields = ('phone_number',)
