from .models import CustomUser, Code
from django.db.models.signals import post_save
from django.dispatch import receiver

# i dont use this signal


@receiver(post_save, sender=CustomUser)
def generate_code(sender, instance, created, *args, **kwargs):
    if created:
        Code.objects.create(user=instance)
