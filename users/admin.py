from django.contrib import admin
from .models import CustomUser, Code
from django.contrib.auth.models import Group

admin.site.unregister(Group)
