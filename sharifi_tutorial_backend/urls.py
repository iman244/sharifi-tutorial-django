from django.contrib import admin
from django.urls import path, include

admin.site.site_header = "IELTS Online Shopping Adminstration"
admin.site.site_title = "IELTS Online Shopping"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('tinymce/', include('tinymce.urls')),
    path('api/auth/', include('users.urls', namespace='otp')),
    path('api/store/', include('store.urls', namespace='store')),
]
