from pathlib import Path
import environ
import os
from datetime import timedelta

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


env = environ.Env(
    # set casting, default value
    DEBUG=(bool, True)
)
# Take environment variables from .env file
environ.Env.read_env(os.path.join(BASE_DIR, '.env'))

# False if not in os.environ because of casting above
DEBUG = os.getenv('DEBUG', True)

# Raises Django's ImproperlyConfigured
# exception if SECRET_KEY not in os.environ
SECRET_KEY = os.getenv('SECRET_KEY', 'SECRET_KEY is not set.')


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_filters',
    'rest_framework',
    'rest_framework.authtoken',
    "phonenumber_field",
    'corsheaders',
    "storages",
    "tinymce",
    'store',
    'users'
]


# CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True

ALLOWED_HOSTS = ['*']
# ALLOWED_HOSTS = ['192.168.1.78', '127.0.0.1', 'localhost', '18.197.157.239',
#                  'https://ieltsonlineshopping1.liara.run', '185.208.181.142']


CORS_ALLOW_METHODS = [
    'DELETE',
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
]

CORS_ALLOWED_ORIGINS = [
    'http://localhost:3000',
    'http://127.0.0.1:3000',
    'http://192.168.1.78:3000',
    'https://ieltsonlineshopping1.liara.run',
    'https://www.ieltsonlineshopping.ir',
    'https://ieltsonlineshopping.ir',
    'https://185.208.181.142'
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'sharifi_tutorial_backend.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'sharifi_tutorial_backend.wsgi.application'


# Database
# https://docs.djangoproject.com/en/5.0/ref/settings/#databases

DATABASES = {
    'default': {
        # 'ENGINE': 'django.db.backends.sqlite3',
        # 'NAME': BASE_DIR / 'db.sqlite3',
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.getenv('DATABASE_NAME', 'DATABASE_NAME is not set.'),
        "USER":  os.getenv('DATABASE_USERNAME', 'DATABASE_USERNAME is not set.'),
        "PASSWORD": os.getenv('DATABASE_PASSWORD_USER', 'DATABASE_PASSWORD_USER is not set.'),
        "HOST": os.getenv('DATABASE_ENDPOINT', 'DATABASE_ENDPOINT is not set.'),
        "PORT": os.getenv('DATABASE_PORT', 'DATABASE_PORT is not set.')
    }
}


# Password validation
# https://docs.djangoproject.com/en/5.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = 'users.CustomUser'

# Internationalization
# https://docs.djangoproject.com/en/5.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True


# Default primary key field type
# https://docs.djangoproject.com/en/5.0/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.TokenAuthentication',
    ], 'DEFAULT_FILTER_BACKENDS': ['django_filters.rest_framework.DjangoFilterBackend']
}

SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=10),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=1),
    'ROTATE_REFRESH_TOKENS': True,
    'BLACKLIST_AFTER_ROTATION': True
}


AWS_S3_OBJECT_PARAMETERS = {
    'Expires': 'Thu, 31 Dec 2099 20:00:00 GMT',
    'CacheControl': 'max-age=94608000',
}
DEFAULT_FILE_STORAGE = "storages.backends.s3boto3.S3Boto3Storage"
AWS_ACCESS_KEY_ID = os.getenv(
    'AWS_ACCESS_KEY_ID', '7418d5e6-d059-4070-b993-4fba59e4ba20')
AWS_SECRET_ACCESS_KEY = os.getenv(
    'AWS_SECRET_ACCESS_KEY', '5f61dbdbc0bc93b7ec5ea69677d9ae53b57bb94133a2207802687a24e17849a6')
AWS_STORAGE_BUCKET_NAME = os.getenv(
    'AWS_STORAGE_BUCKET_NAME', 'ieltsonlineshopping')
AWS_S3_ENDPOINT_URL = os.getenv(
    'AWS_S3_ENDPOINT_URL', 'https://s3.ir-thr-at1.arvanstorage.ir')
print("AWS_STORAGE_BUCKET_NAME----", AWS_STORAGE_BUCKET_NAME)
print("AWS_S3_ENDPOINT_URL----", AWS_S3_ENDPOINT_URL)
# above items need for abr arvan s3
AWS_S3_SIGNATURE_NAME = 's3v4'
AWS_S3_REGION_NAME = 'eu-central-1'
AWS_S3_FILE_OVERWRITE = False
AWS_DEFAULT_ACL = 'public-read'
AWS_S3_VERITY = True


# s3 static settings
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/5.0/howto/static-files/
AWS_S3_PROVIDER_URL = os.getenv(
    'AWS_S3_PROVIDER_URL', 'AWS_S3_PROVIDER_URL is not set.')
AWS_S3_CUSTOM_DOMAIN = f'{AWS_STORAGE_BUCKET_NAME}.{AWS_S3_PROVIDER_URL}'
AWS_S3_OBJECT_PARAMETERS = {'CacheControl': 'max-age=86400'}
STATIC_URL = f'https://{AWS_S3_CUSTOM_DOMAIN}/static/'
STATICFILES_STORAGE = 'sharifi_tutorial_backend.storage_backends.StaticStorage'
TINYMCE_JS_URL = f'{STATIC_URL}tinymce/tinymce.min.js'

# sms web service
KAVENEGAR_API_KEY = os.getenv(
    'KAVENEGAR_API_KEY', 'KAVENEGAR_API_KEY is not set.')

# payment web service
ZARINPAL_MERCHANT_ID = os.getenv(
    'ZARINPAL_MERCHANT_ID', 'ZARINPAL_MERCHANT_ID is not set.')
# zarinpal
SANDBOX = True
